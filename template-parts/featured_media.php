<?php
/*
Template part for adding a featured image, slider, audio or video to pages, posts and media properties.
*/
?>

<div class="featured_media">
	<?php 
		// Featured Image
		if(get_field('featured_image')): 
	?>
		<img src="<?php the_field('featured_image'); ?>" />
	<?php 
		// Featured Video Embed
		elseif (get_field('featured_video')): 
	?>
		<?php the_field('featured_video'); ?>
	<?php 
		// Featured Audio
		elseif(get_field('featured_audio')): 
	?>
		<div class="audio">
			<?php the_field('featured_audio'); ?>
		</div>
	<?php 
		// Featured Slider Images
		elseif( have_rows('futuro_slider') ): 
	?>
		<div id="futuro_slider" class="flexslider">
				<ul class="slides">
				<?php while( have_rows('futuro_slider') ): the_row(); ?>
				    <li>
				      <img src="<?php the_sub_field('futuro_slide_image'); ?>" />
				    </li>
				<?php endwhile; ?>
			</ul>
		</div>
	<?php endif; ?>
</div>