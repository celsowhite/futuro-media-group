<?php
/*
Template part for repeatable half column blocks used across the site.
*/
?>

<div class="futuro_row futuro_blocks">
	<?php while( have_rows('futuro_blocks') ): the_row(); ?>
		<div class="column_1_2 stretch_height">
			<?php if(get_sub_field('futuro_block_link')): ?>

				<!-- If the block links to a page -->

				<a href="<?php the_sub_field('futuro_block_link'); ?>">
					<?php if(get_sub_field('futuro_block_image')): ?>
						<img class="futuro_block_image" src="<?php the_sub_field('futuro_block_image'); ?>" />
					<?php endif; ?>
					<div class="content">
						<h3 class="uppercase"><?php the_sub_field('futuro_block_title'); ?></h3>
						<p class="large"><?php the_sub_field('futuro_block_text'); ?></p>
						<div class="external_news_link">
							<p class="small">Learn more</p>
							<div class="arrow_image">
								<img src="<?php echo get_template_directory_uri() . '/img/long_right_arrow_grey.png' ?>">
							</div>
						</div>
					</div>
				</a>

			<?php else: ?>

				<!-- Else it's a static block that doesn't link anywhere -->
				<a>
					<?php if(get_sub_field('futuro_block_image')): ?>
						<img  class="futuro_block_image" src="<?php the_sub_field('futuro_block_image'); ?>" />
					<?php endif; ?>
					<div class="content">
						<h3 class="uppercase"><?php the_sub_field('futuro_block_title'); ?></h3>
						<p class="large margin_bottom_0"><?php the_sub_field('futuro_block_text'); ?></p>
					</div>
				</a>

			<?php endif; ?>
		</div>
	<?php endwhile; ?>
</div>