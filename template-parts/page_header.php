<?php
/*
Template part for page headers.
Conditionally shows various header types depending if the user added a header image or excerpt text.
*/
?>

<header class="page_header">
	<?php if(get_field('futuro_header_image')): ?>
		<h3 class="orange_tag"><?php the_title(); ?></h3>
	<?php endif; ?>
	<div class="header_content">
		<?php if(!get_field('futuro_header_image')): ?>
			<h1 <?php if(!has_excerpt()): ?>class="margin_bottom_0"<?php endif; ?>><?php the_title(); ?></h1>
		<?php endif; ?>
		<?php if(has_excerpt()): ?>
			<?php the_excerpt(); ?>
		<?php endif; ?>
		<?php if(get_field('media_website_link')): ?>
			<a class="transparent_button" href="http://<?php the_field('media_website_link'); ?>" target="_blank"><?php the_field('media_website_link'); ?></a>
		<?php endif; ?>
	</div>
</header>