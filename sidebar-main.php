<?php
/*====================  
Main sidebar template 
Used on single posts and pages
====================*/
?>

<!-- Support Module -->

<div class="sidebar_module">
	<h2>Support</h2>
	<?php if(get_field('custom_support_text')): ?>
		<p><?php the_field('custom_support_text'); ?></p>
	<?php else: ?>
		<p><?php the_field('sitewide_support_text','option'); ?></p>
	<?php endif; ?>
	<a class="gold_button" href="<?php echo get_page_link(107); ?>">Become A Sponsor</a>
</div>

<!-- Donate Module -->

<div class="sidebar_module">
	<h2>Donate</h2>
	<?php if(get_field('custom_donate_text')): ?>
		<p><?php the_field('custom_donate_text'); ?></p>
	<?php else: ?>
		<p><?php the_field('sitewide_donate_text','option'); ?></p>
	<?php endif; ?>
	<a class="gold_button" href="<?php echo get_page_link(105); ?>">Donate Today</a>
</div>

<!-- 3 Most Recent News Posts -->

<?php 
	$news_args = array('post_type' => 'post', 'posts_per_page' => 3, 'order' => 'DSC', 'order_by' => 'date');
	$news_loop = new WP_Query($news_args);
	if ( $news_loop->have_posts() ):
?>

	<div class="sidebar_module">
		<h2>What's New</h2>
		<?php while ( $news_loop->have_posts() ) : $news_loop->the_post(); ?>
			<?php if (get_field('news_type') === 'external'): ?>
			<a class="sidebar_news_post_container" href="<?php the_field('news_link'); ?>" target="_blank">
			<?php else: ?>
			<a class="sidebar_news_post_container" href="<?php the_permalink(); ?>">
			<?php endif; ?>
				<div class="image">
					<?php the_post_thumbnail('thumbnail'); ?>
				</div>
				<div class="text">
					<p class="title"><?php the_title(); ?></p>
					<p class="date_media_outlet"><span class="date"><?php echo get_the_date(); ?></span> / <?php the_field('news_media_outlet'); ?></p>
				</div>
			</a>
		<?php endwhile; ?>	
	</div>

<?php endif; wp_reset_postdata(); ?>