<?php
/**
 * The header for our theme.
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package _s
 */

?><!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
<meta charset="<?php bloginfo( 'charset' ); ?>">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="google-site-verification" content="XPWExv99EWMQxms7w7a41WLBcOcFG-A5AwPETgGgRJw" />
<link rel="profile" href="http://gmpg.org/xfn/11">
<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">

<!-- Favicon -->

<link rel="icon" type="img/png" href="<?php echo get_template_directory_uri() . '/favicon.png'; ?>" />

<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
	<a class="skip-link screen-reader-text" href="#content"><?php esc_html_e( 'Skip to content', '_s' ); ?></a>
	<?php if (get_field('header_banner_image','option')): ?>
		
		<!-- Optional Header Banner -->

		<div class="header_banner">
			<a href="<?php the_field('header_banner_link', 'option'); ?>" target="_blank">
				<img src="<?php the_field('header_banner_image', 'option'); ?>" />
			</a>
		</div>

	<?php endif; ?>
	<header class="site_header" role="banner">
		<nav class="main_navigation" role="navigation">
			<a href="<?php echo esc_url( home_url( '/' ) ); ?>" class="nav_item nav_15 logo">
				<img src="<?php the_field('futuro_logo','option'); ?>" />
			</a>
			<?php 
				$header_links = get_field('main_header_links', 'option');
			?>
			<?php if(isset($header_links[0])): ?>
				<a href="<?php echo $header_links[0]['link_url']; ?>" class="nav_item"><?php echo $header_links[0]['link_name']; ?></a>
			<?php endif; ?>
			<?php if(isset($header_links[1])): ?>
				<a href="<?php echo $header_links[1]['link_url']; ?>" class="nav_item"><?php echo $header_links[1]['link_name']; ?></a>
			<?php endif; ?>
			<?php if(isset($header_links[2])): ?>
				<a href="<?php echo $header_links[2]['link_url']; ?>" class="nav_item"><?php echo $header_links[2]['link_name']; ?></a>
			<?php endif; ?>
			<?php if(isset($header_links[3])): ?>
				<a href="<?php echo $header_links[3]['link_url']; ?>" class="nav_item"><?php echo $header_links[3]['link_name']; ?></a>
			<?php endif; ?>
			<a class="nav_item menu_icon">Menu  <i class="fa fa-bars"></i></a>
			<?php if(isset($header_links[4])): ?>
				<a href="<?php echo $header_links[4]['link_url']; ?>" class="nav_item" target="_blank"><?php echo $header_links[4]['link_name']; ?></a>
			<?php endif; ?>
		</nav>
	</header>

	<?php wp_nav_menu( array( 'menu' => 'Mobile Menu', 'menu_id' => 'mobile_navigation', 'container' => false ) ); ?>

	<div class="preloader">

	</div>

	<div id="content" class="main_content">