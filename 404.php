<?php
/**
 * The template for displaying 404 pages (not found).
 *
 * @link https://codex.wordpress.org/Creating_an_Error_404_Page
 *
 * @package _s
 */

get_header(); ?>

	<main id="main" class="main_wrapper" role="main">

		<div class="page_content">
			<h1>Page not found.</h1>
			<p>Visit our <a href="<?php echo esc_url( home_url( '/' ) ); ?>">homepage.</a></p>
		</div>

	</main>

<?php get_footer(); ?>
