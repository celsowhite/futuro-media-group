var $j = jQuery.noConflict();

$j(document).ready(function() {

	// Call these functions when the html has loaded.

	"use strict"; // Ensure javascript is in strict mode and catches errors.

	/*================================= 
	PRELOADER EFFECT
	=================================*/

	$j('.preloader').delay(600).fadeOut(300, function(){
		$j('#content').addClass('page_loaded');
	});

	/*================================= 
	MODAL POPUP
	=================================*/

	/*=== Click an individual team member ===*/

	$j('.team_member').click(function(){
		var teamName = $j(this).attr('data-name');
		$j('html').addClass('modal_open');
		$j('.modal_popup#' + teamName).addClass('visible');
	});

	/*=== Close the popup ===*/

	$j('.modal_close, .modal_popup').click(function(){
		$j(this).closest('.modal_popup').removeClass('visible');
		$j('html').removeClass('modal_open');
	});

	/*================================= 
	MOBILE MENU DRAWER
	=================================*/

	$j('a.nav_item.menu_icon').click(function(e){
		e.preventDefault();
		$j('ul#mobile_navigation').toggleClass('open');
	});

	/*================================= 
	DONATION FORM FUNCTIONALITY
	=================================*/

	$j('.frequency_selection li.gchoice_2_14_0').addClass('frequency_on');

	$j('.frequency_selection li.gchoice_2_14_0').click(function(){

		$j('.frequency_selection li.gchoice_2_14_0').addClass('frequency_on');
		$j('.frequency_selection li.gchoice_2_14_1').removeClass('frequency_on');

	});

	$j('.frequency_selection li.gchoice_2_14_1').click(function(){

		$j('.frequency_selection li.gchoice_2_14_0').removeClass('frequency_on');
		$j('.frequency_selection li.gchoice_2_14_1').addClass('frequency_on');

	});

	/*================================= 
	SOCIAL SHARE
	=================================*/

	/*== Facebook Share ==*/

	$j('a.fb_share').click(function(e) {

		e.preventDefault();

		var loc = $j(this).attr('href');

		window.open('http://www.facebook.com/sharer.php?u=' + loc,'facebookwindow','height=450, width=550, top='+($j(window).height()/2 - 225) +', left='+$j(window).width()/2 +', toolbar=0, location=0, menubar=0, directories=0, scrollbars=0');

	});

	$j('a.twitter_share').click(function(e){

	    e.preventDefault();

	    var loc = $j(this).attr('href');

	    var via = $j(this).attr('via');

	    var title  = encodeURIComponent($j(this).attr('title'));

	    window.open('http://twitter.com/share?url=' + loc + '&via=' + via, 'twitterwindow', 'height=450, width=550, top='+($j(window).height()/2 - 225) +', left='+$j(window).width()/2 +', toolbar=0, location=0, menubar=0, directories=0, scrollbars=0');

	});

	/*=== LinkedIn Share ===*/

	$j('a.linkedin_share').click(function(e){

		e.preventDefault();

		var loc = $j(this).attr('href');

		var title = encodeURIComponent($j(this).attr('title'));

		var excerpt  = encodeURIComponent($j(this).attr('excerpt'));

		window.open('https://www.linkedin.com/shareArticle?mini=true&url=' + loc + '&title=' + title + '&summary=' + excerpt);

	});

	/*================================= 
	FITVIDS
	=================================*/

	/*=== Wrap All Iframes with 'video_embed' for responsive videos ===*/

	$j('iframe[src*="youtube.com"], iframe[src*="vimeo.com"]').wrap("<div class='video_embed'/>");

	/*=== Target div for fitVids ===*/

	$j(".video_embed").fitVids();

	/*================================= 
	FLEXSLIDER
	=================================*/

	/*=== Futuro Slider ===*/

	$j('#futuro_slider').flexslider({
    	animation: "fade",
    	controlNav: false,
    	prevText: "",
    	nextText: "", 
    	smoothHeight: true
    });

    /*=== Homepage Slider ===*/

	$j('#homepage_slider').flexslider({
    	animation: "fade",
    	controlNav: false,
    	prevText: "",
    	nextText: ""
    });

    /*=== Testimonial Slider ===*/

	$j('#testimonial_slider').flexslider({
    	animation: "fade",
    	controlNav: false,
    	prevText: "",
    	nextText: "",
    	smoothHeight: true
    });

    /*================================= 
	MAILCHIMP AJAX
	=================================*/

	// Set up form variables

	var mailchimpForm = $j('.mc-embedded-subscribe-form');

	// On submit of the form send an ajax request to mailchimp for data.

	mailchimpForm.submit(function(e){

		// Set variables for this specific form

		var that = $j(this);
		var mailchimpSubmit = $j(this).find('input[type=submit]');
		var errorResponse = $j(this).closest('.mc_embed_signup').find('.mce-error-response');
		var successResponse = $j(this).closest('.mc_embed_signup').find('.mce-success-response');

		// Make sure the form doesn't link anywhere on submit.

		e.preventDefault();

		// JQuery AJAX request http://api.jquery.com/jquery.ajax/

		$j.ajax({
		  method: 'GET',
		  url: that.attr('action'),
		  data: that.serialize(),
		  dataType: 'jsonp',
		  success: function(data) {
			// If there was an error then show the error message.
			if (data.result === 'error') {
				// Hide the first few characters int the error message string which display the error code and hyphen.
				var messageWithoutCode = data.msg.slice(3);
				errorResponse.text(messageWithoutCode).fadeIn(300).delay(3000).fadeOut(300);
			}
			// If success then show message
			else {
				successResponse.text('Success! Please check your email for a confirmation message.').fadeIn(300).delay(3000).fadeOut(300);
			}
		  }
		});
	});

});