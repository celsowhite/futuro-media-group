<?php
/*
Template Name: Team
*/

get_header(); ?>

	<main id="main" class="main_wrapper" role="main">

		<?php while ( have_posts() ) : the_post(); ?>

			<?php get_template_part('template-parts/page_header'); ?>

			<section class="page_content">

				<!-- Production Team -->

				<h2>Production Team</h2>

				<section class="futuro_row team_grid">

					<?php
						$production_team_args = array('post_type' => 'futuro_people', 'futuro_people_category' => 'production-team', 'posts_per_page' => -1, 'order' => 'DSC', 'order_by' => 'date');
						$production_team_loop = new WP_Query($production_team_args);
						if ( $production_team_loop->have_posts() ) : while ( $production_team_loop->have_posts() ) : $production_team_loop->the_post();
					?>

						<?php get_template_part('template-parts/team_member'); ?>

					<?php endwhile; wp_reset_postdata(); endif; ?>

				</section>

				<!-- Administrative Team -->

				<h2>Administrative Team</h2>

				<section class="futuro_row team_grid">

					<?php
						$admin_team_args = array('post_type' => 'futuro_people', 'futuro_people_category' => 'administrative-team', 'posts_per_page' => -1, 'order' => 'DSC', 'order_by' => 'date');
						$admin_team_loop = new WP_Query($admin_team_args);
						if ( $admin_team_loop->have_posts() ) : while ( $admin_team_loop->have_posts() ) : $admin_team_loop->the_post();
					?>

						<?php get_template_part('template-parts/team_member'); ?>

					<?php endwhile; wp_reset_postdata(); endif; ?>

				</section>

			</section>

		<?php endwhile;?>

	</main>

<?php get_footer(); ?>
