<?php
/*
Template Name: Individual Donors
*/

get_header(); ?>
	
	<?php if(get_field('futuro_header_image')): ?>
		<header class="header_image">
			<img src="<?php the_field('futuro_header_image'); ?>" />
		</header>
	<?php endif; ?>

	<main id="main" class="main_wrapper <?php if(get_field('futuro_header_image')): ?>with_header_image<?php endif; ?>" role="main">

		<?php while ( have_posts() ) : the_post(); ?>

			<?php get_template_part('template-parts/page_header'); ?>

			<div class="futuro_row">
				<div class="column_2_3 futuro_main_column">

					<?php if(have_rows('major_donors')): ?>
					
						<!-- Major Donors -->

						<div class="content">
							<h2>Major Donors</h2>
							<div class="futuro_row">
								<?php while( have_rows('major_donors') ): the_row(); ?>
									<div class="column_1_2">
										<?php the_sub_field('major_donors_name'); ?>
									</div>
								<?php endwhile; ?>
							</div>
						</div>

					<?php endif; ?>

					<?php if(have_rows('familia_giving_circle')): ?>

						<!-- Familia Giving Circle -->

						<div class="content">
							<h2>Familia Giving Circle</h2>
							<div class="futuro_row">
								<?php while( have_rows('familia_giving_circle') ): the_row(); ?>
									<div class="column_1_2">
										<?php the_sub_field('familia_giving_circle_name'); ?>
									</div>
								<?php endwhile; ?>
							</div>
							<a href="<?php echo get_page_link(189); ?>" class="external_news_link">
								<p class="small">Learn more about the Familia Giving Circle</p>
								<div class="arrow_image">
									<img src="<?php echo get_template_directory_uri() . '/img/long_right_arrow_grey.png' ?>"></i>
								</div>
							</a>
						</div>

					<?php endif; ?>

					<?php if(have_rows('monthly_sustaining_donor')): ?>

						<!-- Monthly Sustaining Donor -->

						<div class="content">
							<h2>Monthly Sustaining Donors</h2>
							<div class="futuro_row">
								<?php while( have_rows('monthly_sustaining_donor') ): the_row(); ?>
									<div class="column_1_2">
										<?php the_sub_field('monthly_sustaining_donor_name'); ?>
									</div>
								<?php endwhile; ?>
							</div>
						</div>

					<?php endif; ?>
				</div>
				<div class="column_1_3 futuro_sidebar">
					<?php get_sidebar('main'); ?>
				</div>
			</div>

		<?php endwhile;?>

	</main>

<?php get_footer(); ?>
