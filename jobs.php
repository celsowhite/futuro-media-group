<?php
/*
Template Name: Jobs
*/

get_header(); ?>

	<?php if(get_field('futuro_header_image')): ?>
		<header class="header_image">
			<img src="<?php the_field('futuro_header_image'); ?>" />
		</header>
	<?php endif; ?>

	<main id="main" class="main_wrapper <?php if(get_field('futuro_header_image')): ?>with_header_image<?php endif; ?>" role="main">

		<?php while ( have_posts() ) : the_post(); ?>

			<?php get_template_part('template-parts/page_header'); ?>

			<div class="page_content">
				<div class="futuro_row">
					<div class="column_2_3 futuro_main_column futuro_blocks">
						<?php
							$jobs_args = array('post_type' => 'futuro_jobs', 'posts_per_page' => -1, 'order' => 'DSC', 'order_by' => 'date');
							$jobs_loop = new WP_Query($jobs_args);
							if ( $jobs_loop->have_posts() ) : while ( $jobs_loop->have_posts() ) : $jobs_loop->the_post();
						?>
							<a href="<?php the_permalink(); ?>">
								<div class="content">
									<h2><?php the_title(); ?></h2>
									<?php the_excerpt(); ?>
									<div class="external_news_link">
										<p class="small">Learn More</p>
										<div class="arrow_image">
											<img src="<?php echo get_template_directory_uri() . '/img/long_right_arrow_grey.png' ?>"></i>
										</div>
									</div>
								</div>
							</a>
						<?php endwhile; wp_reset_postdata(); ?>

						<?php else: ?>
							<div class="content">
								<h2>No current positions</h2>
								<p>Please check back for more openings!</p>
							</div>
						<?php endif; ?>
					</div>
					<div class="column_1_3 futuro_sidebar">
						<?php get_sidebar('main'); ?>
					</div>
				</div>
			</div>

		<?php endwhile; ?>

	</main>

<?php get_footer(); ?>

