<?php
/*
Template Name: Sponsors
*/

get_header(); ?>
	
	<?php if(get_field('futuro_header_image')): ?>
		<header class="header_image">
			<img src="<?php the_field('futuro_header_image'); ?>" />
		</header>
	<?php endif; ?>

	<main id="main" class="main_wrapper <?php if(get_field('futuro_header_image')): ?>with_header_image<?php endif; ?>" role="main">

		<?php while ( have_posts() ) : the_post(); ?>

			<?php get_template_part('template-parts/page_header'); ?>

			<div class="futuro_row">
				<div class="column_2_3 futuro_main_column">
					<div class="content">
						<?php while( have_rows('futuro_funder') ): the_row(); ?>
							<div class="column_1_3">
								<?php if(get_sub_field('funder_link')): ?>
									<a href="<?php the_sub_field('funder_link'); ?>" target="_blank">
										<img src="<?php the_sub_field('funder_logo'); ?>" />
									</a>
								<?php else: ?>
									<img src="<?php the_sub_field('funder_logo'); ?>" />
								<?php endif; ?>
							</div>
						<?php endwhile; ?>
					</div>
				</div>
				<div class="column_1_3 futuro_sidebar">
					<?php get_sidebar('main'); ?>
				</div>
			</div>

		<?php endwhile;?>

	</main>

<?php get_footer(); ?>
