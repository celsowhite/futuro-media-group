<?php
/*
Template Name: News
*/

get_header(); ?>

	<?php if(get_field('futuro_header_image')): ?>
		<header class="header_image">
			<img src="<?php the_field('futuro_header_image'); ?>" />
		</header>
	<?php endif; ?>

	<main id="main" class="main_wrapper <?php if(get_field('futuro_header_image')): ?>with_header_image<?php endif; ?>" role="main">

		<?php while ( have_posts() ) : the_post(); ?>

			<header class="page_header">
				<div class="header_content">
					<h1 <?php if(!has_excerpt()): ?>class="margin_bottom_0"<?php endif; ?>><?php the_title(); ?></h1>
					<?php if(has_excerpt()): ?>
						<?php the_excerpt(); ?>
					<?php endif; ?>
				
					<!-- Post archive dropdown -->

					<section class="futuro_date_select">
						<select name="archive-dropdown" onchange="document.location.href=this.options[this.selectedIndex].value;">
						  <option value="">Filter By Date</option> 
						  <?php wp_get_archives( array( 'type' => 'monthly', 'format' => 'option', 'show_post_count' => 1 ) ); ?>
						</select>
					</section>
				</div>
			</header>

			<!-- Ajax Load More News Posts -->

			<?php echo do_shortcode('[ajax_load_more post_type="post" posts_per_page="12" scroll="false" css_classes="futuro_row futuro_blocks" button_label="Load More" button_loading_label="Loading News ..."]'); ?>

		<?php endwhile;?>

	</main>

<?php get_footer(); ?>
