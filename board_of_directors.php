<?php
/*
Template Name: Board Of Directors
*/

get_header(); ?>

	<main id="main" class="main_wrapper" role="main">

		<?php while ( have_posts() ) : the_post(); ?>

			<header class="page_header">
				<div class="header_content">
					<h1 <?php if(!the_excerpt()): ?>class="margin_bottom_0"<?php endif; ?>><?php the_title(); ?></h1>
					<?php the_excerpt(); ?>
				</div>
			</header>

			<section class="page_content">

				<section class="futuro_row team_grid">

					<?php
						$board_members_args = array('post_type' => 'futuro_people', 'futuro_people_category' => 'board-of-directors', 'posts_per_page' => -1, 'order' => 'DSC', 'order_by' => 'date');
						$board_members_loop = new WP_Query($board_members_args);
						if ( $board_members_loop->have_posts() ) : while ( $board_members_loop->have_posts() ) : $board_members_loop->the_post();
					?>

						<?php get_template_part('template-parts/team_member'); ?>

					<?php endwhile; wp_reset_postdata(); endif; ?>

				</section>

			</section>

		<?php endwhile;?>

	</main>

<?php get_footer(); ?>
