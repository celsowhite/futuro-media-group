<?php
/**
 * _s functions and definitions.
 *
 * @link https://codex.wordpress.org/Functions_File_Explained
 *
 * @package _s
 */

if ( ! function_exists( '_s_setup' ) ) :

/**
 * Sets up theme defaults and registers support for various WordPress features.
 *
 * Note that this function is hooked into the after_setup_theme hook, which
 * runs before the init hook. The init hook is too late for some features, such
 * as indicating support for post thumbnails.
 */
function _s_setup() {
	/*
	 * Make theme available for translation.
	 * Translations can be filed in the /languages/ directory.
	 * If you're building a theme based on _s, use a find and replace
	 * to change '_s' to the name of your theme in all the template files
	 */
	load_theme_textdomain( '_s', get_template_directory() . '/languages' );

	// Add default posts and comments RSS feed links to head.
	add_theme_support( 'automatic-feed-links' );

	/*
	 * Let WordPress manage the document title.
	 * By adding theme support, we declare that this theme does not use a
	 * hard-coded <title> tag in the document head, and expect WordPress to
	 * provide it for us.
	 */
	add_theme_support( 'title-tag' );

	/*
	 * Enable support for Post Thumbnails on posts and pages.
	 *
	 * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
	 */
	add_theme_support( 'post-thumbnails' );

	// Image size for news posts thumbnails

	add_image_size( 'news-thumb', 600, 360, true );

	// This theme uses wp_nav_menu() in one location.
	register_nav_menus( array(
		'primary' => esc_html__( 'Primary Menu', '_s' ),
	) );

	/*
	 * Switch default core markup for search form, comment form, and comments
	 * to output valid HTML5.
	 */
	add_theme_support( 'html5', array(
		'search-form',
		'comment-form',
		'comment-list',
		'gallery',
		'caption',
	) );

	/*
	 * Disable support for Post Formats. This theme currently doesn't need it.
	 * See https://developer.wordpress.org/themes/functionality/post-formats/
	add_theme_support( 'post-formats', array(
		'aside',
		'image',
		'video',
		'quote',
		'link',
	) );
	*/

	// Set up the WordPress core custom background feature.
	add_theme_support( 'custom-background', apply_filters( '_s_custom_background_args', array(
		'default-color' => 'ffffff',
		'default-image' => '',
	) ) );
}
endif; // _s_setup
add_action( 'after_setup_theme', '_s_setup' );

/**
 * Set the content width in pixels, based on the theme's design and stylesheet.
 *
 * Priority 0 to make it available to lower priority callbacks.
 *
 * @global int $content_width
 */
function _s_content_width() {
	$GLOBALS['content_width'] = apply_filters( '_s_content_width', 640 );
}
add_action( 'after_setup_theme', '_s_content_width', 0 );

/*==========================================
ENQUEUE SCRIPTS AND STYLES
==========================================*/

function _s_scripts() {
	wp_enqueue_style( '_s-style', get_stylesheet_uri() );

	/*=== Font Awesome ===*/

	wp_enqueue_style('font-awesome', '//maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css' );

	/*=== Source Sans Pro Google Font ===*/

	wp_enqueue_style('source-sans-pro', 'https://fonts.googleapis.com/css?family=Source+Sans+Pro:400,400italic,600,700,700italic,900' );

	/*=== Flexslider Styles ===*/

	wp_enqueue_style('flexslider', get_template_directory_uri() . '/css/flexslider/flexslider.min.css');

	/*=== Animate CSS ===*/

	wp_enqueue_style('animate_css', get_template_directory_uri() . '/css/animate/animate.min.css');

	/*=== FitVids ===*/

	wp_enqueue_script('_s-fitvids', get_template_directory_uri() . '/js/fitvids/fitvids.min.js', '', '', true);

	/*=== Flexslider JS ===*/

	wp_enqueue_script( '_s-flexslider', get_template_directory_uri() . '/js/flexslider/jquery.flexslider-min.js', '','', true);

	/*=== Custom Scripts ===*/

	wp_enqueue_script('_s-scripts', get_template_directory_uri() . '/js/scripts.js', '', '', true);

	/*=== Modernizr (Flexbox & Geolocation Detection) ===*/

	wp_enqueue_script( '_s-modernizr', get_template_directory_uri() . '/js/modernizr/modernizr-custom.min.js', '','', true);

	wp_enqueue_script( '_s-skip-link-focus-fix', get_template_directory_uri() . '/js/skip-link-focus-fix.js', array(), '20130115', true );

	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	}

	/*=== Compiled SCSS File ===*/

	wp_enqueue_style( 'custom_styles', get_template_directory_uri() . '/css/style.min.css' );

	/*=== Wordpress Default Jquery ===*/

	if (!is_admin()) {
		wp_enqueue_script('jquery');
	}

}

add_action( 'wp_enqueue_scripts', '_s_scripts' );

/*==========================================
CUSTOM POST TYPES
==========================================*/

/*=== Properties ===*/

function custom_post_type_properties() {

	$labels = array(
		'name'                => _x( 'Media Properties', 'Post Type General Name'),
		'singular_name'       => _x( 'Media Property', 'Post Type Singular Name'),
		'menu_name'           => __( 'Media Properties'),
		'parent_item_colon'   => __( ''),
		'all_items'           => __( 'All Properties'),
		'view_item'           => __( 'View Property'),
		'add_new_item'        => __( 'Add New Media Property'),
		'add_new'             => __( 'Add New'),
		'edit_item'           => __( 'Edit Property'),
		'update_item'         => __( 'Update Property'),
		'search_items'        => __( 'Search Properties'),
		'not_found'           => __( 'Not Found'),
		'not_found_in_trash'  => __( 'Not found in Trash'),
	);
	
	$args = array(
		'label'               => __( 'properties'),
		'description'         => __( 'Futuro media properties'),
		'labels'              => $labels,
		'supports'            => array( 'title', 'editor', 'excerpt', 'thumbnail', 'page-attributes' ),
		'hierarchical'        => false,
		'rewrite'             => array('slug' => 'media-property'),
		'taxonomies'          => array( 'futuro_property_category' ),
		'public'              => true,
		'show_ui'             => true,
		'show_in_menu'        => true,
		'show_in_nav_menus'   => true,
		'show_in_admin_bar'   => true,
		'menu_position'       => 5,
		'can_export'          => true,
		'has_archive'         => true,
		'exclude_from_search' => false,
		'publicly_queryable'  => true,
		'capability_type'     => 'post',
		'menu_icon'  		  => 'dashicons-admin-site',
	);

	register_post_type( 'futuro_properties', $args );

}

add_action( 'init', 'custom_post_type_properties', 0 );

/*=== People (Team & Board) ===*/

function custom_post_type_people() {

	$labels = array(
		'name'                => _x( 'People', 'Post Type General Name'),
		'singular_name'       => _x( 'People', 'Post Type Singular Name'),
		'menu_name'           => __( 'People'),
		'parent_item_colon'   => __( ''),
		'all_items'           => __( 'All People'),
		'view_item'           => __( 'View Person'),
		'add_new_item'        => __( 'Add New Person'),
		'add_new'             => __( 'Add New'),
		'edit_item'           => __( 'Edit Person'),
		'update_item'         => __( 'Update Person'),
		'search_items'        => __( 'Search People'),
		'not_found'           => __( 'Not Found'),
		'not_found_in_trash'  => __( 'Not found in Trash'),
	);
	
	$args = array(
		'label'               => __( 'people'),
		'description'         => __( 'Futuro People'),
		'labels'              => $labels,
		'supports'            => array( 'title', 'editor', 'thumbnail', 'page-attributes', 'excerpt'  ),
		'hierarchical'        => false,
		'rewrite'             => array('slug' => 'people'),
		'taxonomies'          => array( 'futuro_people_category' ),
		'public'              => true,
		'show_ui'             => true,
		'show_in_menu'        => true,
		'show_in_nav_menus'   => true,
		'show_in_admin_bar'   => true,
		'show_in_rest'        => true,
		'menu_position'       => 5,
		'can_export'          => true,
		'has_archive'         => true,
		'exclude_from_search' => false,
		'publicly_queryable'  => true,
		'capability_type'     => 'post',
		'menu_icon'  		  => 'dashicons-groups',
	);

	register_post_type( 'futuro_people', $args );

}

add_action( 'init', 'custom_post_type_people', 0 );

/*=== Jobs ===*/

function custom_post_type_jobs() {

	$labels = array(
		'name'                => _x( 'Jobs', 'Post Type General Name'),
		'singular_name'       => _x( 'Job', 'Post Type Singular Name'),
		'menu_name'           => __( 'Jobs'),
		'parent_item_colon'   => __( ''),
		'all_items'           => __( 'All Jobs'),
		'view_item'           => __( 'View Job'),
		'add_new_item'        => __( 'Add New Job'),
		'add_new'             => __( 'Add New'),
		'edit_item'           => __( 'Edit Job'),
		'update_item'         => __( 'Update Job'),
		'search_items'        => __( 'Search Jobs'),
		'not_found'           => __( 'Not Found'),
		'not_found_in_trash'  => __( 'Not found in Trash'),
	);
	
	$args = array(
		'label'               => __( 'jobs'),
		'description'         => __( 'Futuro jobs'),
		'labels'              => $labels,
		'supports'            => array( 'title', 'editor', 'thumbnail', 'page-attributes', 'excerpt'  ),
		'hierarchical'        => false,
		'rewrite'             => array('slug' => 'job'),
		'public'              => true,
		'show_ui'             => true,
		'show_in_menu'        => true,
		'show_in_nav_menus'   => true,
		'show_in_admin_bar'   => true,
		'menu_position'       => 5,
		'can_export'          => true,
		'has_archive'         => true,
		'exclude_from_search' => false,
		'publicly_queryable'  => true,
		'capability_type'     => 'post',
		'menu_icon'  		  => 'dashicons-welcome-write-blog',
	);

	register_post_type( 'futuro_jobs', $args );

}

add_action( 'init', 'custom_post_type_jobs', 0 );

/*==========================================
CUSTOM TAXONOMY 
==========================================*/

/*=== Properties Taxonomy ===*/

function add_properties_taxonomy() {

  register_taxonomy('futuro_property_category', 'futuro_properties', array(

      'hierarchical' => true,
      'labels' => array(
      'name' => _x( 'Categories', 'taxonomy general name' ),
      'singular_name' => _x( 'Category', 'taxonomy singular name' ),
      'search_items' =>  __( 'Search Categories' ),
      'all_items' => __( 'All Categories' ),
      'parent_item' => __( 'Parent Category' ),
      'parent_item_colon' => __( 'Parent Category:' ),
      'edit_item' => __( 'Edit Category' ),
      'update_item' => __( 'Update Category' ),
      'add_new_item' => __( 'Add New Category' ),
      'new_item_name' => __( 'New Category Name' ),
      'menu_name' => __( 'Categories' ),
    ),

      'rewrite' => array(
      'slug' => 'category', 
      'with_front' => false,
    ),
  ));
}
add_action( 'init', 'add_properties_taxonomy', 0 );

/*=== People Taxonomy ===*/

function add_people_taxonomy() {

  register_taxonomy('futuro_people_category', 'futuro_people', array(

      'hierarchical' => true,
      'labels' => array(
      'name' => _x( 'Categories', 'taxonomy general name' ),
      'singular_name' => _x( 'Category', 'taxonomy singular name' ),
      'search_items' =>  __( 'Search Categories' ),
      'all_items' => __( 'All Categories' ),
      'parent_item' => __( 'Parent Category' ),
      'parent_item_colon' => __( 'Parent Category:' ),
      'edit_item' => __( 'Edit Category' ),
      'update_item' => __( 'Update Category' ),
      'add_new_item' => __( 'Add New Category' ),
      'new_item_name' => __( 'New Category Name' ),
      'menu_name' => __( 'Categories' ),
    ),

      'rewrite' => array(
      'slug' => 'category', 
      'with_front' => false,
    ),
  ));
}
add_action( 'init', 'add_people_taxonomy', 0 );

/*=============================================
ACF OPTIONS PAGE
=============================================*/

if( function_exists('acf_add_options_page') ) {

	acf_add_options_page();
	
}

/*==========================================
LIMIT POST REVISIONS
==========================================*/

function limit_post_revisions( $num, $post ) {
    $num = 3;
    return $num;
}

add_filter( 'wp_revisions_to_keep', 'limit_post_revisions', 10, 2 );

/*=============================================
PAGE EXCERPTS
=============================================*/

function add_excerpts_to_pages() {
     add_post_type_support( 'page', 'excerpt' );
}

add_action( 'init', 'add_excerpts_to_pages' );

/*=============================================
CUSTOM LOGIN SCREEN
=============================================*/

// Change the login logo URL

function my_loginURL() {
    return 'http://futuromediagroup.org';
}
add_filter('login_headerurl', 'my_loginURL');

// Enque the login specific stylesheet for design customizations. CSS file is compiled through compass.

function my_logincustomCSSfile() {
    wp_enqueue_style('login-styles', get_template_directory_uri() . '/css/login.min.css');
}
add_action('login_enqueue_scripts', 'my_logincustomCSSfile');

/*==========================================
BUTTON SHORTCODE
==========================================*/

function futuro_button_shortcode( $atts, $content = null ) {

	$a = shortcode_atts( array(
        'url'     => '',
        'new_tab' => 'false'
    ), $atts );

    if($a['new_tab'] === 'true') {
        $new_tab = 'target="_blank"';
    }
    else {
        $new_tab = '';
    }

    return '<a href="' . $a['url'] . '"' . $new_tab . 'class="gold_button">' . $content . '</a>';

}

add_shortcode( 'futuro_button', 'futuro_button_shortcode' );

/*=============================================
YOAST
=============================================*/

// Adjust Metabox Priority

add_filter( 'wpseo_metabox_prio', function() { return 'low';});

/*==========================================
INCLUDES
==========================================*/

/*== Implement the Custom Header feature. ==*/

require get_template_directory() . '/inc/custom-header.php';

/*== Custom template tags for this theme. ==*/

require get_template_directory() . '/inc/template-tags.php';

/*== Custom functions that act independently of the theme templates. ==*/

require get_template_directory() . '/inc/extras.php';

/*== Customizer additions. ==*/

require get_template_directory() . '/inc/customizer.php';

/*== Load Jetpack compatibility file. ==*/

require get_template_directory() . '/inc/jetpack.php';
