<div class="column_1_3 stretch_height">
	<?php if (get_field('news_type') === 'external'): ?>
	<a href="<?php the_field('news_link'); ?>" target="_blank">
	<?php else: ?>
	<a href="<?php the_permalink(); ?>">
	<?php endif; ?>
        <?php the_post_thumbnail('news-thumb', array( 'class'  => 'futuro_block_image' )); ?>
		<div class="content">
			<h3 class="uppercase"><?php echo get_the_date(); ?></h3>
			<p class="news_title"><?php the_title(); ?></p>
			<?php if (get_field('news_type') === 'external'): ?>
				<div class="external_news_link">
					<p class="small">View more on <?php the_field('news_media_outlet'); ?></p>
					<div class="arrow_image">
						<img src="<?php echo get_template_directory_uri() . '/img/long_right_arrow_grey.png' ?>"></i>
					</div>
				</div>
			<?php endif; ?>
		</div>
	</a>
</div>