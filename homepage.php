<?php
/*
Template Name: Homepage
*/

get_header(); ?>

	<main id="main post-<?php the_ID(); ?>" class="main_wrapper" role="main">

		<?php while ( have_posts() ) : the_post(); ?>

			<!-- Homepage Top Content -->

			<?php 
			// If they have put content into the backend content editor then show it here.
			if(get_the_content() != ''): ?>

				<?php the_content(); ?>

			<?php 
			// Else show the homepage slider
			else: ?>

				<?php if( have_rows('homepage_slider') ): ?>

					<!-- Homepage Slider -->
						
					<div id="homepage_slider" class="flexslider">
					  
					  <ul class="slides">
						
						<?php while( have_rows('homepage_slider') ): the_row(); ?>

						    <li>
						    	<?php $post_object = get_sub_field('homepage_slide_link'); ?>
						    	<a href="<?php echo get_permalink($post_object->ID); ?>">
									<img src="<?php the_sub_field('homepage_slide_image'); ?>" />
								</a>
								<?php if(!get_sub_field('simple_slide')): ?>
							      <div class="slide_container">
							      	  <div class="slide_content">
									      <h3 class="orange_tag small"><?php echo $post_object->post_title; ?></h3>
									      <h1><?php the_sub_field('homepage_slide_caption'); ?></h1>
									  </div>
								      <a href="<?php echo get_permalink($post_object->ID); ?>" class="transparent_button">Learn More <img src="<?php echo get_template_directory_uri() . '/img/long_right_arrow_white.png' ?>"></a>
								  </div>
								<?php endif; ?>
						    </li>

						<?php endwhile; ?>
					  
					  </ul>

					</div>

				<?php endif; ?>

			<?php endif; ?>

			<!-- Our Purpose -->

			<section class="homepage_about">
				<h3 class="yellow uppercase"><?php the_field('homepage_about_title'); ?></h3>
				<p class="large white"><?php the_field('homepage_about_text'); ?></p>
				<img src="<?php echo get_template_directory_uri() . '/img/homepage_purpose_icons.png'; ?>" />
			</section>

			<!-- Homepage Clickable Blocks -->

			<?php get_template_part('template-parts/futuro_blocks'); ?>

			<!-- Recent News & Twitter -->

			<div class="futuro_row futuro_blocks">
				<div class="column_2_3 stretch_height homepage_newsfeed">
					<div class="title">
						<h2>Recent News</h2>
						<a href="<?php echo get_page_link(103); ?>">View All News</a>
					</div>
					<?php
						$news_args = array('post_type' => 'post', 'posts_per_page' => 2, 'order' => 'DSC', 'order_by' => 'date');
						$news_loop = new WP_Query($news_args);
						if ( $news_loop->have_posts() ) : while ( $news_loop->have_posts() ) : $news_loop->the_post();
					?>
						<?php if (get_field('news_type') === 'external'): ?>
						<a class="homepage_newsfeed_block" href="<?php the_field('news_link'); ?>" target="_blank">
						<?php else: ?>
						<a class="homepage_newsfeed_block" href="<?php the_permalink(); ?>">
						<?php endif; ?>
							<div class="content">
								<h3 class="uppercase"><?php echo get_the_date(); ?></h3>
								<p class="news_title"><?php the_title(); ?></p>
								<?php if (get_field('news_type') === 'external'): ?>
									<div class="external_news_link">
										<p class="small">View more on <?php the_field('news_media_outlet'); ?></p>
										<div class="arrow_image">
											<img src="<?php echo get_template_directory_uri() . '/img/long_right_arrow_grey.png' ?>">
										</div>
									</div>
								<?php endif; ?>
							</div>
						</a>
					<?php endwhile; endif; wp_reset_postdata(); ?>
				</div>
				<div class="column_1_3 homepage_twitter_feed">
					<a class="twitter-timeline" href="https://twitter.com/celsostories/lists/futuro-media-group" data-height="300" data-chrome="noheader nofooter transparent" data-widget-id="733661489131868160">Tweets from https://twitter.com/celsostories/lists/futuro-media-group</a>
					<script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?'http':'https';if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src=p+"://platform.twitter.com/widgets.js";fjs.parentNode.insertBefore(js,fjs);}}(document,"script","twitter-wjs");</script>
				</div>
			</div>
		<?php endwhile; ?>

	</main>

<?php get_footer(); ?>
