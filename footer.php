<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package _s
 */

?>

</div>

<footer class="footer">

	<!-- Footer top with nav menus -->

	<section class="footer_top">
		<div class="container">
			<div class="footer_logo">
				<a href="<?php echo esc_url( home_url( '/' ) ); ?>">
					<img src="<?php the_field('futuro_white_logo','option'); ?>" />
				</a>
			</div>
			<div class="footer_text">
				<?php the_field('futuro_footer_text','option'); ?>
				<div class="mc_embed_signup">
				    <form action="//futuromediagroup.us2.list-manage.com/subscribe/post-json?u=4a536459ac050fe60bfcdddd7&amp;id=636c56063c&c=?" method="post" name="mc-embedded-subscribe-form" class="validate mc-embedded-subscribe-form trago_mailchimp_form" novalidate>
				      <input type="email" value="" name="EMAIL" class="required email" placeholder="Join Our Mailing List" id="mce-EMAIL"><input type="submit" value="Submit" name="submit" id="mc-embedded-subscribe" class="button">
				      <!-- real people should not fill this in and expect good things - do not remove this or risk form bot signups-->
				      <div style="position: absolute; left: -5000px;" aria-hidden="true"><input type="text" name="b_bfaeb9f9cfe94bf88ebe82105_58d7ecad1c" tabindex="-1" value=""></div>
				    </form>
				    <div class="mce-responses" class="clear">
				      <div class="response mce-error-response" style="display:none"></div>
				      <div class="response mce-success-response" style="display:none"></div>
				    </div>
				</div>
			</div>
			<nav class="footer_nav">
				<?php wp_nav_menu( array( 'menu' => 'Footer Menu #1', 'container' => '' ) ); ?>
			</nav>
			<nav class="footer_nav">
				<?php wp_nav_menu( array( 'menu' => 'Footer Menu #2', 'container' => '' ) ); ?>
			</nav>
			<ul class="social_icons">
				<li><a href="https://twitter.com/futuromedia" target="_blank"><i class="fa fa-twitter"></i></a></li>
				<li><a href="https://www.facebook.com/TheFuturoMediaGroup" target="_blank"><i class="fa fa-facebook"></i></a></li>
			</ul>
		</div>
	</section>

	<!-- Footer bottom with links to media properties -->
	
	<section class="footer_bottom">
		<div class="container">
			<h3 class="uppercase">Our Media Properties</h3>
			<nav class="media_properties_nav">
				<?php wp_nav_menu( array( 'menu' => 'Media Properties', 'container' => '') ); ?>
			</nav>
		</div>
	</section>

</footer>

<?php wp_footer(); ?>

<!-- Google Analytics Pulled from Options Page Created by ACF -->

<?php if(get_field('google_analytics','option')): ?>

	<script>
		var _gaq = _gaq || [];
		_gaq.push(['_setAccount', '<?php the_field('google_analytics','option'); ?>']);
		_gaq.push(['_trackPageview']);

		(function() {
			var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
			ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
			var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
		})();
	</script>

<?php endif; ?>

</body>
</html>
