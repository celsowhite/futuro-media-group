<?php
/*
Template Name: Media Properties
*/

get_header(); ?>
	
	<?php if(get_field('futuro_header_image')): ?>
		<header class="header_image">
			<img src="<?php the_field('futuro_header_image'); ?>" />
		</header>
	<?php endif; ?>

	<main id="main" class="main_wrapper <?php if(get_field('futuro_header_image')): ?>with_header_image<?php endif; ?>" role="main">

		<?php while ( have_posts() ) : the_post(); ?>

			<?php get_template_part('template-parts/page_header'); ?>

			<div class="futuro_row futuro_blocks">
				
				<?php

					// Loop through individual media properties. Order dictated by Intuitive CPT plugin

					$media_properties_args = array('post_type' => 'futuro_properties', 'posts_per_page' => -1, 'order' => 'DSC', 'order_by' => 'date');
					$media_properties_loop = new WP_Query($media_properties_args);
					if ( $media_properties_loop->have_posts() ) : while ( $media_properties_loop->have_posts() ) : $media_properties_loop->the_post();
				?>
					<div class="column_1_2 stretch_height">
						<a href="<?php the_permalink(); ?>">
							<div class="thumbnail_image">
								<?php the_post_thumbnail(); ?>
							</div>
							<div class="content">
								<div class="media_property_logo">
									<img src="<?php the_field('media_property_logo'); ?>" />
								</div>
								<h2><?php the_title(); ?></h2>
								<p class="large"><?php echo get_the_excerpt(); ?></p>
								<?php if(get_field('media_property_partnership_logo')): ?>
									<div class="partnership">
										<h3 class="uppercase"><?php the_field('media_property_partnership_text'); ?></h3> <img src="<?php the_field('media_property_partnership_logo'); ?>" />
									</div>
								<?php endif; ?>
							</div>
						</a>
					</div>

				<?php endwhile; wp_reset_postdata(); endif; ?>

			</div>

		<?php endwhile;?>

	</main>

<?php get_footer(); ?>
