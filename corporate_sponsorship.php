<?php
/*
Template Name: Corporate Sponsorship
*/

get_header(); ?>
	
	<?php if(get_field('futuro_header_image')): ?>
		<header class="header_image">
			<img src="<?php the_field('futuro_header_image'); ?>" />
		</header>
	<?php endif; ?>

	<main id="main" class="main_wrapper <?php if(get_field('futuro_header_image')): ?>with_header_image<?php endif; ?>" role="main">

		<?php while ( have_posts() ) : the_post(); ?>

			<header class="page_header margin_bottom_0">
				<div class="header_content center">
					<h1><?php the_title();?></h1>
					<p><?php echo get_the_excerpt(); ?></p>
				</div>
			</header>

			<!-- Sponsorship Advantages -->

			<section class="sponsorship_advantages">
				<div class="advantage_block">
					<img src="<?php echo get_template_directory_uri() . '/img/corporate_program_branded_messages_icons.png' ?>" />
					<p class="large">Our Programs</p>
				</div>
				<div class="operator_block">
					<img src="<?php echo get_template_directory_uri() . '/img/corporate_program_plus.png' ?>" />
				</div>
				<div class="advantage_block">
					<img src="<?php echo get_template_directory_uri() . '/img/corporate_program_branded_messages_icons.png' ?>" />
					<p class="large">Your Branded Messages</p>
				</div>
				<div class="operator_block">
					<img src="<?php echo get_template_directory_uri() . '/img/corporate_program_equal.png' ?>" />
				</div>
				<div class="advantage_block">
					<img src="<?php echo get_template_directory_uri() . '/img/corporate_program_our_listeners_icons.png' ?>" />
					<p class="large">Our Audience, Your Customers</p>
				</div>
			</section>

			<!-- Audience -->

			<section class="our_audience white_background_section">
				<h2 class="center margin_bottom_40">Our Audience</h2>
				<div class="futuro_row">
					<?php if(have_rows('our_audience_statistics')): ?>

						<?php while(have_rows('our_audience_statistics')): the_row(); ?>

							<div class="column_1_3 center">
								<h1 class="orange"><?php the_sub_field('our_audience_big_stat'); ?></h1>
								<p><?php the_sub_field('our_audience_text'); ?></p>
							</div>

						<?php endwhile; ?>

					<?php endif; ?>
				</div>
			</section>

			<!-- Our Reach -->

			<section class="our_reach white_background_section">
				<h2 class="center margin_bottom_40">Highlights of Our Reach</h2>
				<div class="futuro_row">
					<?php if(have_rows('our_reach_media_properties')): ?>

						<?php while(have_rows('our_reach_media_properties')): the_row(); ?>

							<div class="column_1_3 center">
								<img src="<?php the_sub_field('our_reach_logo'); ?>" />
								<p><?php the_sub_field('our_reach_text'); ?></p>
							</div>

						<?php endwhile; ?>

					<?php endif; ?>
				</div>
			</section>

			<!-- Testimonials -->

			<section class="audience_thoughts grey_background_section">
				<h2 class="white center margin_bottom_40">What Our Audience is Saying</h2>
				<div id="testimonial_slider" class="flexslider">
				  	<ul class="slides">
						<?php while( have_rows('futuro_testimonials') ): the_row(); ?>
						    <li>
						      <div class="testimonial_content">
						      	  <p><?php the_sub_field('futuro_testimonial_text'); ?></p>
						      	  <h3 class="white uppercase"><?php the_sub_field('futuro_testimonial_name'); ?></h3>
							  </div>
						    </li>
						<?php endwhile; ?>
					</ul>
				</div>
			</section>

			<!-- Become A Sponsor CTA Block -->

			<section id="become_a_sponsor" class="white_background_section">
				<div class="content">
					<?php the_content(); ?>
				</div>
			</section>

		<?php endwhile;?>

	</main>

<?php get_footer(); ?>
