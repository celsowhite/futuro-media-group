<?php
/**
 * The template for displaying all single media properties
 */

get_header(); ?>

	<?php if(get_field('futuro_header_image')): ?>
		<header class="header_image">
			<img src="<?php the_field('futuro_header_image'); ?>" />
		</header>
	<?php endif; ?>

	<main id="main" class="main_wrapper <?php if(get_field('futuro_header_image')): ?>with_header_image<?php endif; ?>" role="main">

		<?php while ( have_posts() ) : the_post(); ?>

			<?php get_template_part('template-parts/page_header'); ?>

			<div class="page_content">
				<div class="futuro_row">
					<div class="column_2_3 futuro_main_column">
						<?php get_template_part('template-parts/featured_media'); ?>
						<div class="content">
							<?php the_content(); ?>
						</div>
						<?php if(have_rows('futuro_blocks')): ?>
							<h2>The Stats</h2>
							<?php get_template_part('template-parts/futuro_blocks'); ?>
						<?php endif; ?>
					</div>
					<aside class="column_1_3 futuro_sidebar">
						<?php get_sidebar('media_property'); ?>
					</aside>
				</div>

				<!-- Explore Media Properties -->
				
				<h2>Explore Our Other Media Properties</h2>

				<div class="futuro_row explore_our_properties">
					<?php
						$current_media_property_ID = $post->ID;
						$media_properties_args = array('post_type' => 'futuro_properties', 'posts_per_page' => -1, 'order' => 'DSC', 'order_by' => 'date', 'post__not_in' => array($current_media_property_ID));
						$media_properties_loop = new WP_Query($media_properties_args);
						if ( $media_properties_loop->have_posts() ) : while ( $media_properties_loop->have_posts() ) : $media_properties_loop->the_post();
					?>
						<div class="column_1_3 related_media_property">
							<a href="<?php the_permalink(); ?>" class="">
								<?php the_post_thumbnail(); ?>
								<div class="logo">
									<img src="<?php the_field('media_property_logo'); ?>" />
								</div>
							</a>
						</div>
					<?php endwhile; endif; wp_reset_postdata(); ?>
				</div>

			</div>

		<?php endwhile; ?>

	</main>

<?php get_footer(); ?>