<?php
/*
Template Name: About
*/

get_header(); ?>

	<?php if(get_field('futuro_header_image')): ?>
		<header class="header_image">
			<img src="<?php the_field('futuro_header_image'); ?>" />
		</header>
	<?php endif; ?>

	<main id="main" class="main_wrapper <?php if(get_field('futuro_header_image')): ?>with_header_image<?php endif; ?>" role="main">

		<?php while ( have_posts() ) : the_post(); ?>

			<?php get_template_part('template-parts/page_header'); ?>

			<div class="page_content">
				<div class="futuro_row">
					<div class="column_2_3 futuro_main_column">

						<?php get_template_part('template-parts/featured_media'); ?>

						<div class="content">
							<?php the_content(); ?>
						</div>

						<?php get_template_part('template-parts/futuro_blocks'); ?>

					</div>
					<div class="column_1_3 futuro_sidebar">
						<?php get_sidebar('main'); ?>
					</div>
				</div>
			</div>

		<?php endwhile; ?>

	</main>

<?php get_footer(); ?>
