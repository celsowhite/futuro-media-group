Futuro Media Group Theme
===

A custom Wordpress theme created for futuromediagroup.org.

Installation
===

- Download the newest version of Wordpress, install your database and setup the wp-config file. I recommend using wp-cli for these steps.
- Download or clone this repository into wp-content/themes.
- Activate the theme.
- Make sure node, sass and gulp are installed on your local machine.
- Run npm install.
- Run gulp.
- Start coding your new theme!
