<?php
	/*=============
	Template for showing team members and board of directors images and modals
	=============*/

	// Set variable for team members name to be used for calling a specific team member popup
	$team_name = $post->post_name;
?>

<div class="column_1_4">
	<div class="team_member" data-name="<?php echo $team_name; ?>">
		<?php the_post_thumbnail(); ?>
		<h3 class="name"><?php the_title(); ?></h3>
	</div>
</div>

<!-- Modal -->

<div class="modal_popup" id="<?php echo $team_name; ?>">
	<div class="container">
		<div class="futuro_row">
			<div class="column_1_4">
				<div class="team_member">	
					<?php the_post_thumbnail(); ?>
				</div>
			</div>
			<div class="column_3_4">	
				<h2><?php the_title(); ?></h2>
				<h3><?php the_field('futuro_role'); ?></h3>
				<?php the_content(); ?>
				<ul class="social_icons">
					<?php if(get_field('people_twitter_link')): ?>
						<li><a href="<?php the_field('people_twitter_link'); ?>" target="_blank"><i class="fa fa-twitter"></i></a></li>
					<?php endif; ?>
					<?php if(get_field('people_facebook_link')): ?>
						<li><a href="<?php the_field('people_facebook_link'); ?>" target="_blank"><i class="fa fa-facebook"></i></a></li>
					<?php endif; ?>
					<?php if(get_field('people_instagram_link')): ?>
						<li><a href="<?php the_field('people_instagram_link'); ?>" target="_blank"><i class="fa fa-instagram"></i></a></li>
					<?php endif; ?>
				</ul>
			</div>
		</div>
		<div class="modal_close">
			<span class="close_icon"></span>
		</div>
	</div>
</div>