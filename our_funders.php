<?php
/*
Template Name: Our Funders
*/

get_header(); ?>
	
	<?php if(get_field('futuro_header_image')): ?>
		<header class="header_image">
			<img src="<?php the_field('futuro_header_image'); ?>" />
		</header>
	<?php endif; ?>

	<main id="main" class="main_wrapper <?php if(get_field('futuro_header_image')): ?>with_header_image<?php endif; ?>" role="main">

		<?php while ( have_posts() ) : the_post(); ?>

			<?php get_template_part('template-parts/page_header'); ?>

			<div class="futuro_row futuro_blocks">
				<?php while( have_rows('futuro_blocks') ): the_row(); ?>
					<div class="column_1_3 stretch_height">
						<a href="<?php the_sub_field('futuro_block_link'); ?>">
							<div class="content">
								<h3 class="uppercase"><?php the_sub_field('futuro_block_title'); ?></h3>
								<p class="large"><?php the_sub_field('futuro_block_text'); ?></p>
								<div class="external_news_link">
									<p class="small">Learn more</p>
									<div class="arrow_image">
										<img src="<?php echo get_template_directory_uri() . '/img/long_right_arrow_grey.png' ?>">
									</div>
								</div>
							</div>
						</a>
					</div>
				<?php endwhile; ?>
			</div>

		<?php endwhile;?>

	</main>

<?php get_footer(); ?>
