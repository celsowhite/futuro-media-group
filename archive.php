<?php
/**
 * The template for displaying archive pages.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package _s
 */

get_header(); ?>

	<main id="main" class="main_wrapper" role="main">

		<header class="page_header">
			<div class="header_content">
				<h1 class="margin_bottom_0"><?php the_archive_title();?></h1>
			
				<!-- Post archive dropdown -->

				<section class="futuro_date_select">
					<select name="archive-dropdown" onchange="document.location.href=this.options[this.selectedIndex].value;">
					  <option value="">Filter By Date</option> 
					  <?php wp_get_archives( array( 'type' => 'monthly', 'format' => 'option', 'show_post_count' => 1 ) ); ?>
					</select>
				</section>
			</div>
		</header>

		<div class="futuro_row futuro_blocks">

			<?php while ( have_posts() ) : the_post(); ?>

				<div class="column_1_3 stretch_height">
					<?php if (get_field('news_type') === 'external'): ?>
					<a href="<?php the_field('news_link'); ?>" target="_blank">
					<?php else: ?>
					<a href="<?php the_permalink(); ?>">
					<?php endif; ?>
						<?php the_post_thumbnail('news-thumb', array( 'class'  => 'futuro_block_image' )); ?>
						<div class="content">
							<h3 class="uppercase"><?php echo get_the_date(); ?></h3>
							<p class="news_title"><?php the_title(); ?></p>
							<?php if (get_field('news_type') === 'external'): ?>
								<div class="external_news_link">
									<p class="small">View more on <?php the_field('news_media_outlet'); ?></p>
									<div class="arrow_image">
										<img src="<?php echo get_template_directory_uri() . '/img/long_right_arrow_grey.png' ?>"></i>
									</div>
								</div>
							<?php endif; ?>
						</div>
					</a>

				</div>

			<?php endwhile;?>

		</div>

	</main>

<?php get_footer(); ?>
